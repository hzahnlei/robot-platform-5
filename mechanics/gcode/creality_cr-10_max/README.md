# Pre-Sliced G-code for Creality3D CR-10 Max

- Sliced with Cura 4.1 for Creality CR-10 Max.
- Most objects are sliced for 0.8mm nozzle.
- Only the speaker grille and mount have been sliced for 0.4mm nozzle for the sake of precision and detail.
