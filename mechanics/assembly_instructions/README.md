# Mechanics Assembly Instructions

## Introduction

First, all mechanical parts and their function will be described.
Then, some pictures are used to give a better idea of how to mount the robot body.

Most parts are to be 3D printed.
Many parts are quite big and require a printer with large build volumes such as Creality3D CR-10 Max.

Some parts from the shelf are used in addition to the printed parts.
Such parts are axles, ball bearings, screws, brass inserts etc.

## Mechanical Parts List

| Name                | Function |
| ------------------- | -------- |
| `Arm Stub`          | Just a lid to cover the arm sockets until arms are available. |
| `Battery Lid`       | Big lid/flap to cover the battery compartment access opening at the back/bottom. |
| `Bottom Sensor Lid` | Lid to cover access opening at front/bottom. Also intended to carry collision detection sensors. |
| `Cam Mount`         | Intel Realsense depth and tracking cameras are to be mounted here. Part is mounted to front of head. |
| `Caster Mount`      | Print two times. Mount caster wheels at front and back of robot. |
| `Collar`            | Collar that carries the head. To be mounted on top of body. |
| `Compute Lid`       | Lid/flap for closing the compute module bays at back of robot. Print three times. |
| `Dual Slide`        | One generic slide-in (full height). Compute modules can be mounted here and slide into the compute bay at back of robot. This is a generic part. There might be the need to drill holes to mount a compute model. Alternatively dedicated slide-in can be derived from this and 3D printed. |
| `Fender`            | A part to cover the drive wheel. Print two times. |
| `Head Axis`         | The head rotating mechanism. Nema 17 motor and rotary encoder are mounted here. |
| `Head Bottom`       | Lower bowl of the head. Microphones are to be mounted here. |
| `Head Carrier`      | Kind of a grid. Rotates around middle axis (mounted to `Head Axis`). Mounted to `Head Bottom` and thereby carrying the head. |
| `Head Stops`        | Mechanically limiting the degree of freedom by which the head can rotate. To be mounted to `Head Axis` and `Head Carrier`. |
| `Head Top`          | Upper bowl of the head. Mainly a cover to protect the heads interior. |
| `Jetson Nano Slide` | A full hight slight-in with mounting holes for mounting a Jetson Nano development kit.  |
| `Lower Frame`       | A stabilizing grid to be mounted at the bottom of `Section 3`. Also carries the `Vertical Mount`s for compute modules. |
| `Mic Case`          | A capsule to hold a microphone. Print four times. Mounted to `Head Bottom`. The file `mic_cases_4_v2.gcode` already contains all four mic capsules. |
| `Mini Power Pack`   | A simple construction to hold the 25V battery. It also carries Buck converters and screw terminals. The braces are screwed to the bottom of `Section 1`. The power pack can be slid-in to the battery compartment at the bottom/back of the robot. It is held in place by the to braces. It is then secured with two M4 screws at the back. |
| `Nema 17 Adapter`   | Carries a Neam 17 motor for driving the robot. Print two times. |
| `Section 1`         | Lower part of the robot body. |
| `Section 2`         | Part of the robot body. |
| `Section 3`         | Part of the robot body. |
| `Section 4`         | Part of the robot body. |
| `Section 5`         | Upper part of the robot body. |
| `Single Slide`      | Two generic slide-ins (half height). Compute modules can be mounted here and slide into the compute bay at back of robot. This is a generic part. There might be the need to drill holes to mount a compute model. Alternatively dedicated slide-in can be derived from this and 3D printed. |
| `Speaker`           | A speaker is mounted to this part. The part is the inserted from the outside into the intake at the front of the robot. Inside it is secured with screws. |
| `Stabilizer`        | Screwed to all three vertical mounts to stabilize the cage that holds the compute module slide-ins. |
| `Top Sensor Lid`    | Lid/flap to cover the front/top access opening of the robot body. Also intended to carry further distance measurung sensors. |
| `Upper Frame`       | Another stabilizing grid. Mounted to `Section 4`. May also be used for additional anchoring future arms here. |
| `Vertical Mount`    | Carries compute modules. Print three times. Mounted to `Lower Frame`. Stabilized by `Stabilizer`. |

## The Body

Segments are glued and screwed together.

<img src="./segments_glued_together.png"/>

Gaps are filled with putty.
Also the surface gets smoothened with putty.
(And a lot of sanding.)

<img src="./filling_gaps_with_putty.jpg"/>

A first coat of silver color is covering the body.
Some weathering has been applied already.

<img src="./body_with_first_coat.jpg"/>

## Access Openings and Lids

Magnets are used for holding lids/flaps in place.
The 3D printed parts already feature indented/recessed areas to fit 10mm diameter neodymium magnets in there.
The magnets are glued with epoxy.

<img src="./magnets_for_holding_lids_in_place_01.png"/>

<img src="./magnets_for_holding_lids_in_place_02.png"/>

## Head

<img src="./head_mechanism_01.jpg"/>

<img src="./head_mechanism_02.jpg"/>

<img src="./head_mechanism_03.jpg"/>

<img src="./head_mechanism_04.jpg"/>

<img src="./head_mechanism_05.jpg"/>

## Motors

<img src="./motor_adapter.png"/>

## Power Supply

At the back, the power pack is secured with M4 screws.

<img src="./power_pack_mounted_in_robot.png"/>

At the front, the power pack is slid under the holding braces.
Therefore, it cannot move to the sides nor to the front.

<img src="./power_pack_helt_by_braces.png"/>
