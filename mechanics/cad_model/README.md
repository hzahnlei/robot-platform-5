# CAD Model

The mechanical design has been done using the free license of Fusion360.
This is a free, non-commercial and open source project after all.

Actually, I designed the robot twice.
The first version was printable but I made many design mistakes.
The walls were to thin so the model did bend/warp too much.
Some parts did not fit together exactly etc.
I not only had to learn CAD design but also about 3D printing as this is my first 3D print experience.
One should not underestimate the learning effort.

In any case, the model has been exported to STEP format as not everybody wants to use Fusion360.
Fusion360 is a great CAD tool.
However, I had to renew the free license after some time.
The kill features in the free version from time to time.
So, a good alternative is [FreeCAD](https://www.freecadweb.org).
It is feature rich, free and open source.
Plenty of tutorials are available for free.
