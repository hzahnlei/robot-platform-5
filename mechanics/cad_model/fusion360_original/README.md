# CAD Design Files

These are the original CAD design files.

The robot parts have been designed separately as separate components.
Each component has been designed in the location it belongs.
So all parts can be put together in the CAD software.

The `master` file contains most of the parameters.
For example, the robot's diameter needs to be known almost everywere.
The same goes for parameters like M4 drill hole diameter etc.

All other components have been designed by first importing the `master`.
