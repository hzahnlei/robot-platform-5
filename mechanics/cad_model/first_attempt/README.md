= First Attempt

2020-09-29, Holger Zahnleiter

The files herein represent my first attempt to model the robot body using Fusion 360.

It was printable.
But the middle segment did not come out of the printer perfectly.
The walls were too thin given the prints size.
It did not keep its shape.

I made other errors as well.
For example, the diameter of the drill holes for M4 threaded brass inserts were slightly to small.

I finally decided to redesign the robot body from scratch and benefit from the lessons learned and from my Fusion 360 skills I had gained.

== C.A.D. Files

The original Fusion 360 files are stored in the `fusion360` folder.

== G-code

The folder `gcode` contains G-code files that were actually printed.
All files are intended for the Creality CR-10 Max printer.
Everything is sliced for a 0.8mm nozzel and PLA.
Only the loud speaker is sliced for a 0.4mm nozzel.
Nozzel temperature is 200°C, hot bed temperature is 60°.
