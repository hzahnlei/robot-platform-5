# Mechanics

## Introduction

The mechanical design was created with Autodesk Fusion 360.
Ultimaker Cura 4.8.0 has been used for slicing.
The robot body has been printed using a Creality CR-10 Max.
This printer allows for particularly large prints.

The body has been sliced into five segments.
These have been stacked and screwed and glued together to form the body.
The seems have been filled and primed.
The same has been done to the grooves resulting from 3D printing.
Then a lot of sanding, priming, sanding, priming followed.
Finally everything was painted using mostly spray paint from cans and some air brush for the details.
Oil paints and washes have been used for the weathering and aging.
Dry brush techniques were involved as well as dabbing with towels.
(Overall the finish was the most time consuming part.
Even though the 3D design took longer than I had expected.
I generally underestimated the effort.)

<img src="./body_made_up_of_sections.png"/>

Lids are giving access to the interior so that electronics and mechanical parts can be mounted inside.
The lids are held in place by Neodymium magnets.
They can easily be opened/closed.
No complex mechanics (hinges) required.

<img src="./openings_and_mounting_points.png"/>

The mechanical design has been made flexible in that different motor mounts can be designed and printed.
For example, in the current version of the robot body a motor mount for Nema 17 motors is included.
As these are not very powerful and noisy on top of that it is quite likely that a future incarnation of the desigh will use brushless motors instead.
In this case a new motor mount has to be designed and printed.
Then simply exchange the existings mounts and motors be the new ones - et voilà.
