# External Software

This folder contains software that was not developed by me.
Of course one can install the latest versions of Arduino libraries and such from the internet.
I am providing installable packages here for the sake of completeness.
