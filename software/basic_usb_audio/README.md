# Basic USB Audio Sketch for Teensy 4.1

## Introduction

A basic USB audio device.
No gating, no audio enhancements etc.

**This is actually part of the robotic platform delivered by this project.**

## How to Build and Upload

1. Open the sketch in the Teensyduino IDE.
2. From the board types select "Teensy 4.1".
3. As USB type select "Audio".
4. Select the corresponding USB port (different on different machines and operating systems).
5. Press "compile and upload".
6. Once the upload is completed:
   - Select "Teensy Audio" as audio source and destination.
   - Use recording software (e.g. Audacity) to record audio from the robot's microphones.
   - Use that same recording software to play back the sounds you just recorded.
     The sounds should now emanate from the robots speakers.
