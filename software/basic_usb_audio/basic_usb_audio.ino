/*
   This makes the Teensy act as a bidirectional USB audio device.
   - One mono I2S amplifier is attached to I2S #1.
     It appears as a USP audio output when attached to your computer.
     Left and right channels become mixed to form a mono signal.
     However, the robot only has one speaker anyways.
   - Two I2S MEMS microphones are attached to I2S #2.
     These are exposed as a USB audio input when attached to your computer.
   - The program does not perform any filtering or enhancements.
     It is streight through, hence "basic USB audio".

   For this to work the following has to be done:
   - From the Teensyduino menu select "Tool/USB Type/Audio"
     Otherwise the sketck will not compile.
     The compiler will complain that "AudioInputUSB does not name a type" instead.
   - In this scenario it is required to press the "program" button on the Teensy 4.1 board.
   - Then press "Comile and Upload" Teensyduino IDE.
   - On your computer this device should now appear as USB audio out and USB audio in devices.

   For the output on I2S #1 use these Teensy 4.1 pins:
   Pin    I2S function
   21     BCLK
    7     TX
   20     LRCLK

   For the input on I2S #2 use these Teensy 4.1 pins:
   Pin    I2S function
   4      BCLK
   5      RX
   3      LRCLK
*/

#include <Audio.h>

static constexpr auto LEFT_CHANNEL = 0U;
static constexpr auto RIGHT_CHANNEL = 1U;

AudioInputUSB usb_input;
AudioInputI2S2 i2s2_input;
AudioOutputUSB usb_output;
AudioOutputI2S i2s1_output;
AudioConnection patch_usb_left_in{usb_input, LEFT_CHANNEL, i2s1_output, LEFT_CHANNEL};
AudioConnection patch_usb_right_in{usb_input, RIGHT_CHANNEL, i2s1_output, RIGHT_CHANNEL};
AudioConnection patch_usb_left_out{i2s2_input, LEFT_CHANNEL, usb_output, LEFT_CHANNEL};
AudioConnection patch_usb_right_out{i2s2_input, RIGHT_CHANNEL, usb_output, RIGHT_CHANNEL};

auto setup() -> void
{
    AudioMemory(50); // 50 is just a guess that works.
}

auto loop() -> void
{
    // Intentionally left blank.
}
