# Software

## Introduction

Not all software components contained with this project are actually part of the robotic platform delivered with this project.
Some components are just test or demo components intended to test or demonstrate single features of the robot.
All components are therefore explicitely marked as part or no-part of the platform.

If you want the robot to be operational, then all platform components need to be installed and up and running.
The components are to be installed either on the main computer or the microcontrollers.

## Software Components

- **Test/Demo**
  - [Interrupt Test](./interrupt_test/README.md) - Demonstrate interrupts triggered by sensors.
  - [Stepper Test](./stepper_test/README.md) - Drives head and driving motors to demonstrate use of stepper motors.
- **Platform**
  - [Basic USB Audio](./basic_usb_audio/README.md) - A basic USB audio device.
    No gating, no audio enhancements etc.
- **External**
  - Installable software packes (Arduino libraries) for some select components.
  - Software not written by me but by other people/organizations.
