/*!
    Demonstrates how the robot moves and rotates head using stepper motors and stepper motor drivers.
    Rotates a few degrees counter-clockwise and continuously rotates head.

    2021-12-28, Holger Zahnleiter
*/

#include <AccelStepper.h>

namespace head
{
        constexpr auto PULSE_PIN = 62U;  // Also ADC:8
        constexpr auto DIR_PIN = 63U;    // Also ADC:9
        constexpr auto ENABLE_PIN = 64U; // Also ADC:10
        AccelStepper stepper{AccelStepper::DRIVER, PULSE_PIN, DIR_PIN};
} // namespace head

namespace drive
{

        namespace left
        {
                constexpr auto PULSE_PIN = 65U; // Also ADC:11
                constexpr auto DIR_PIN = 66U;   // Also ADC:12
                AccelStepper stepper{AccelStepper::DRIVER, PULSE_PIN, DIR_PIN};
        } // namespace left

        constexpr auto ENABLE_PIN = 67U; // Also ADC:13

        namespace right
        {
                constexpr auto PULSE_PIN = 68U; // Also ADC:14
                constexpr auto DIR_PIN = 69U;   // Also ADC:15
                AccelStepper stepper{AccelStepper::DRIVER, PULSE_PIN, DIR_PIN};
        } // namespace right
} // namespace drive

void setup()
{
        head::stepper.setEnablePin(head::ENABLE_PIN);
        head::stepper.disableOutputs();

        drive::left::stepper.setEnablePin(drive::ENABLE_PIN);
        drive::left::stepper.disableOutputs();
        drive::right::stepper.disableOutputs();

        delay(10000);

        head::stepper.enableOutputs();
        head::stepper.setMaxSpeed(80);
        head::stepper.setAcceleration(75);
        head::stepper.moveTo(100);

        drive::left::stepper.enableOutputs();
        drive::left::stepper.setMaxSpeed(100);
        drive::left::stepper.setAcceleration(25);
        drive::left::stepper.moveTo(800);

        drive::right::stepper.enableOutputs();
        drive::right::stepper.setMaxSpeed(100);
        drive::right::stepper.setAcceleration(25);
        drive::right::stepper.moveTo(800);
}

void loop()
{
        if (0 == head::stepper.distanceToGo())
        {
                head::stepper.moveTo(-head::stepper.currentPosition());
        }

        if (0 == drive::left::stepper.distanceToGo() || 0 == drive::right::stepper.distanceToGo())
        {
                drive::left::stepper.disableOutputs();
                drive::right::stepper.disableOutputs();
        }

        head::stepper.run();
        drive::left::stepper.run();
        drive::right::stepper.run();
}
