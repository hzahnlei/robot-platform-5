## Introduction

This sketch is uploaded to the controller board.
Running the sketch will result in

- the head rotating continuously clock and counter clockwise,
- the body rotating around its Z axis for one rotation.

**This is actually test/demo software and not part of the robotic platform delivered by this project.**

## How to Build and Upload

1. Open the sketch in the Arduino IDE.
2. From the board types select Arduino Mega 2560.
3. Select the corresponding USB port (different on different machines and operating systems).
4. Press "compile and upload".
5. Once the upload is completed:
   - You have now 5s before movement starts.
   - Switch on the power for the actuators (`SW3`).
   - The robot will now start its movement.
   - You even do not need to have the main computer switched on.
     The sketch is running autonomously on the contoller board and unwind its sequence of movements.
