/*!
    Demonstrates how inductive end-switches of the head are triggering interrupts.
    End switches are attached to digital pins 2 (D2), 3 (D3) and 18 (D18).

    Can also be used to check rotary encoder interrupts.
    Rotary encoder signal A is attached to pin 19 (D19), signal B to pin 21 (D21).

    If an interrupt is triggered, then built-in LED lights up.

    Pick one of the pins by setting END_SWITCH_PIN accordingly.

    2021-12-28, Holger Zahnleiter
*/

constexpr auto RIGHT_END_SWITCH_PIN = 2U;
constexpr auto MIDDLE_END_SWITCH_PIN = 3U;
constexpr auto LEFT_END_SWITCH_PIN = 18U;
constexpr auto ROTARY_ENCODER_A_PIN = 19U;
constexpr auto ROTARY_ENCODER_B_PIN = 21U;

constexpr auto INTERRUPT_PIN = RIGHT_END_SWITCH_PIN;

auto isr() -> void
{
        digitalWrite(LED_BUILTIN, !digitalRead(INTERRUPT_PIN));
}

auto blink_to_indicate_readyness() -> void
{
        for (auto i = 0U; i < 10; i++)
        {
                digitalWrite(LED_BUILTIN, HIGH);
                delay(100);
                digitalWrite(LED_BUILTIN, LOW);
                delay(100);
        }
}

auto setup() -> void
{
        pinMode(LED_BUILTIN, OUTPUT);
        pinMode(INTERRUPT_PIN, INPUT); // Do not use INPUT_PULLUP here.
        blink_to_indicate_readyness();
        attachInterrupt(digitalPinToInterrupt(INTERRUPT_PIN), isr, CHANGE);
}

auto loop() -> void
{
}
