# Interrupt Test

## Introduction

In the sketch, one can pick one of the predefined, interrupt sensitive inputs.
If you rotate the head by hand, then this interrupt will be triggered.

- If you pick one of the inductive end-switches, then you need to rotate the head so that one of the metal screws is moving over the switch.
- If you pick one of the rotary encoder interrupts, then slow rotation of the head will trigger the interrupt slow enough for human observation.

Successfully triggering an interrupt is indicated by the built-in LED on the controller board.

**This is actually test/demo software and not part of the robotic platform delivered by this project.**

## How to Build and Upload

1. Open the sketch in the Arduino IDE.
2. From the board types select Arduino Mega 2560.
3. Select the corresponding USB port (different on different machines and operating systems).
4. Press "compile and upload".
5. Once the upload is completed:
   - Rotate head by hand.
   - Observe built-in LED on controller board.
