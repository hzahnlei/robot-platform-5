# Robotics Platform V

## Introduction

<img src="./banner.png" style="hight: 60%"/>

I have salvaged this SR unit from a crashed freighter on Tatooine.
The blast points where not very accurate.
Therefore, I assume the freighter was not taken down by imperial troops but by pirates.

The SR unit is intended for heavy industrial work.
Hence its bulky appearance and dirty look.

It was in a bad condition due to the battle and the crash.
However, I managed to repair it - despite the rebellion and all.

Have fun,<br>
Holger Zahnleiter<br>
2020-10-12

## Project Goals

The goal of this project is to provide a robotics platform.
The robot is not intended to be a readymade appliance.
Instead, it is meant to provide general services on which one can build robotics applications.
Services provided by the platform are:

- Mechanics
  - Body/case, 3D printable
  - Drive system
  - Rotatable head that carries various sensors
- Electronics
  - Power supply
  - Controllers for driving actuators such as the drive motors or the motor that rotates the head
  - Audio input/output
  - Visual and depth sensors
  - Tracking
  - Compute capacity (including support for deep learning and artificial intelligence)
- Software
  - API for applications to interact with the robotic "body"
    - Read sensors
    - Rotate head
    - Drive around

## Technical Specification

Mobile, autonomous service robot:

-  3D printed body (custom design)
-  Electronic components off the shelf
   - Arduino Mega 2560 compatible to control actuators and read simple sensors
   - Teensy 4.1 as an audio device (stereo microphones and mono speaker)
   - BUCK converters
   - Stepper motor drivers
-  Mechanical components off the shelf
   - M4 and M3 screws
   - Brass inserts
   - Nema 17 stepper motors
   - Ball bearings, gears, belts
-  Uses hardware-accelerated AI (NVIDIA Jetson series)
-  Uses Intel RealSense tracking (T265) and depth cameras (D435)
-  Free and open source

## Documentation

This robot is built from separate subsystems.
These are separately developed and tested and finally integrated into a - surprise - robot.

- [Mechanics](mechanics/README.md)
  - [Assembly Instructions](mechanics/assembly_instructions/README.md)
  - [CAD Model](mechanics/cad_model/README.md)
    - [CAD Design Files](mechanics/cad_model/fusion360_original/README.md)
    - [STEP Export](mechanics/cad_model/step_export/README.md)
  - [G-code](mechanics/gcode/README.md)
    - [Creality3D CR-10 Max](mechanics/gcode/creality_cr-10_max/README.md)
- [Electronics](electronics/README.md)
  - [Assembly Instructions](electronics/assembly_instructions/README.md)
  - [Audio Board](electronics/audio_board/README.md)
  - [Connectors](electronics/connectors/README.md)
  - [Controller Board](electronics/controller_board/README.md)
  - [Main Computer](electronics/main_computer/README.md)
  - [Power Supply](electronics/power_supply/README.md)
  - [Stepper Motor Drivers](electronics/stepper_motor_drivers/README.md)
- [Software](software/README.md)
  - [Basic USB Audio](software/basic_usb_audio/README.md)
  - [Interrupt Test](software/interrupt_test/README.md)
  - [Stepper Test](software/stepper_test/README.md)

## What is Missing

This is a first version of the robot platform.
It is not feature-complete.
The following things would be great extensions and improvements in a future update:

-  Better driving motors (brushless)
-  External jacks for cable based LAN, HDMI and USB (keyboard, mouse)
   - VNC via WLAN is somewhat slow and choppy
-  External connector for charging, possibly in a charging station
-  Battery monitor
-  Arms (sockets are present already, but arms need to be designed and built)

## Times Have Changed

As the name suggests, this is not my first attempt to build a robotics platform.
Main obstacle in my previous attempts was the mechanical part.
However, since the advent of affordable 3D printers I am now able to build precise and complex mechanical parts.

But also electronics components are much more affordable than they were just a few years prior.
And they are much more capable as well.
My first robot had an Intel 80286 clocked at 16MHz.
Deep learning was out of reach.
Later models used Intel Atom CPUs.
But still, the AI performance was much weaker than the performance offered by the Jetson Nano - at the same price.
In earlier models I was using Microsoft's Kinect.
This was a cool device at its time.
However, the newer Intel RealSense cameras offer better features at much less power consumption.
Also software support is much better.

## Disclaimer

This is a free, non commercial and open source project.
You are building this robot and/or its components on your own risk.
I am not responsible for any damage, accidents or injuries it may cause.

Be advised that this is a relatively big and heavy robot.
Lithium ion batteries are dangerous and may ignite when handled improperly.

## Credits

I am thankfully using many free and/or open source libraries, tools and services.
Some I may not even be aware of.

- GitLab
- Microsoft Visual Studio Code
- Arduino IDE
- Teensyduino IDE
- (Ubuntu) GNU/Linux
- GCC
- ...
