# Changelog

## mechanics-1.0.0 (2021-11-09)

- Initial version of mechanics done and printable.
- Robot body assembled and painted.
- Fits together quite well. However, integration with electronics and software may reveal the need to change/adjust mechanics or design additional parts.
