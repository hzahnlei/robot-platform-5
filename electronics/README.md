# Electronics

## Introduction

Instead of building own electronic components I am using ready made components as much as possible.
In the past I built my own microcontroller boards etc.
However, this is quite an effort.
Fully functional boards are available today for a reasonable price.
So I am just wiring together existing components.

These are the components I am using:

- Lithium ion battery with load controller and protection mechanisms, 25V, 10Ah
- Buck convertes to produce 5.0V and 7.5V
- Jetson Nano Development Kit as main computer
- Teensy 4.1 as USB audio device
  - INMP441 I<sup>2</sup>S MEMS microphone breakout boards (stereo)
  - MAX98357A I<sup>2</sup>S audio amplifier breakout board (mono)
- Arduino Mega 2560 compatible as controller
  - Nema 17 stepper motors
  - TB6600 stepper motor drivers

## Communications Concept

The robot implements a control hierachy:

- The main computer is responsible for high-level control, decistion making etc. (superordinate control, überlagerte Steuerung).
- Microcontrollers are responsible for low-level control and measurement (subordinate control, unterlagerte Steuerung).
- The main computer produces and sends commands to the microcontrollers.
- The microcontrollers receive those commands and control actuators and read sensors.
- The microcontrollers also report sensor data back to the main computer if required.
- High-bandwith sensors are directly connected to the main computer.

All communication between controllers and main computer is done via USB.
The controllers are equipped with bootloaders.
Therefore, it is possible to reprogram the controllers directly from the main computer without the need to open the robot, connect cables etc.

<img src="./physical_communication_infrastructure.svg" width="75%"/>
