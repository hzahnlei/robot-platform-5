# Connectors, Switches and Screw Terminals

This is just an overview of all connectors, switches and screw terminals.
Details are to be found in the respective subfolders for each electronic component.

## Switches

| Designation   | Function                                                                               |
| ------------- | -------------------------------------------------------------------------------------- |
| `SW1`         | Main power switch, cuts battery completely off. Microcontrollers are on, if this is on |
| `SW2`         | Main computer power switch, `SW1` needs to be on for this to be effective              |
| `SW3`         | Actuator power switch, `SW1` needs to be on for this to be effective                   |

## Screw Terminals

| Designation   | Function                                                                            |
| ------------- | ----------------------------------------------------------------------------------- |
| `ST1a`        | A screw terminal for GND, completely cut off from battery if `SW1` is off           |
| `ST1b`        | See `ST1b`                                                                          |
| `ST2`         | A screw terminal for battery power, completely cut off from battery if `SW1` is off |
| `ST3`         | 7.5V/300W delivered by a Buck converter                                             |
| `ST4`         | A screw terminal for actuators, completely cut off from battery if `SW3` is off     |

## Connectors

| Designation   | Function                                                   |
| ------------- | ---------------------------------------------------------- |
|               | **Battery Charging**                                       |
| `CN1`         | Charging plug                                              |
|               | **Power Switches**                                         |
| `CN2a`        | Battery to main power plug, connected to battery           |
| `CN2b`        | Battery to main power jack, connected to `SW1`             |
| `CN3a`        | Jack, connects `SW2` (main computer) and `SW3` (actuators) |
| `CN3b`        | Plug, connects `SW2` (main computer) and `SW3` (actuators) |
|               | **Power Supply**                                           |
| `CN4a`        | XT60 jack to power controller board (Arduino)              |
| `CN5a`        | XT60 jack to power USB audio (Teensy, currently not used)  |
| `CN6a`        | XT60 plug to power main computer (Jetson Nano)             |
| `CN7a`        | XT60 jack to power driver for left driving motor           |
| `CN8a`        | XT60 jack to power driver for right driving motor          |
| `CN9a`        | XT60 jack to power driver for head rotating motor          |
| `CN17a`       | XT60 plug to power USB hub                                 |
|               | **Controller Board**                                       |
| `CN4b`        | Power input, connects to `CN4a`                            |
| `CN10a`       | End-switch, head, right                                    |
| `CN11a`       | End-switch, head, middle/front                             |
| `CN12a`       | End-switch, head, left                                     |
| `CN13a`       | Rotary encoder, head                                       |
| `CN14a`       | Signals to stepper driver, head driving motor              |
| `CN15a`       | Signals to stepper driver, left driving motor              |
| `CN16a`       | Signals to stepper driver, right driving motor             |
|               | **Audio Board**                                            |
| `CN18a`       | Left I<sup>2</sup>S MEMS microphone                        |
| `CN19a`       | Right I<sup>2</sup>S MEMS microphone                       |
| `CN20a`       | I<sup>2</sup>S mono amplifier                              |
| `CN21a`       | Speaker                                                    |
|               | **Head Motor/Stepper Driver**                              |
| `CN22a`       | Control signals from controller board                      |
| `CN23a`       | Connection to stepper motor                                |
| `CN24a`       | Connection to power supply                                 |
|               | **Left Drive Motor/Stepper Driver**                        |
| `CN25a`       | Control signals from controller board                      |
| `CN26a`       | Connection to stepper motor                                |
| `CN27a`       | Connection to power supply                                 |
|               | **Right Drive Motor/Stepper Driver**                       |
| `CN28a`       | Control signals from controller board                      |
| `CN29a`       | Connection to stepper motor                                |
| `CN30a`       | Connection to power supply                                 |
