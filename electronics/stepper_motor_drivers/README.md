# Steper Motor Drivers

## Introduction

The current design uses affordable stepper motors and drivers for driving the robot as well as rotating the head.
One motor plus driver did cost about 22EUR.

They are affordable and work quite well, but - let's face it - they are quite noisy as well.
If I do not get this fixed then I will try using brushless motors and controllers.
These are very quiet and powerful, albeit expensive as well.

## Schematics

<img src="./schematics.svg" width="65%"/>
