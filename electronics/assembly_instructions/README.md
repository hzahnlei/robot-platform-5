# Electronics Assembly Instructions

The electronics components are described in detail in their respective `README.md`.
Given here is the general wiring scheme of how to integrate all those components.

<img src="./general_wiring_scheme.svg"/>
