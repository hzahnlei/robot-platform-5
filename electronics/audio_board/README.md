# Audio Board

## Introduction

The audioboard mainly consists of a prototyping board with a socket for a Teensy 4.1.
The Teensy basically does all the work.

It reads a pair of stereo microphones via I<sup>2</sup>S bus.
That data is provided to the main computer by means of USB.

It also reads audio data, provided by the main computer, from the USB bus.
This audi data is then placed on the I<sup>2</sup>S bus.
A mono amplifier is attached to that bus.
Hence, the audio becomes audible for humans through use of a small speaker attached to the amplifier.

## Schematics

<img src="./schematics.svg" width="80%"/>

## Ports Utilization

| Teensy 4.1 Port  | I<sup>2</sup>S Signal | Utilized As                            |
| ---------------- | --------------------- | -------------------------------------- |
|                  |                       | **I<sup>2</sup>S, Device #1**          |
| `7` aka `TX`     | `SD`                  | Data output to amplifier               |
| `20` aka `LRCLK` | `WS`                  | Indicator for left/right audio channel |
| `21` aka `BCLK`  | `SCK`                 | Data clock                             |
|                  |                       | **I<sup>2</sup>S, Device #2**          |
| `5` aka `RX`     | `SD`                  | Data input from microphone             |
| `3` aka `LRCLK`  | `WS`                  | Indicator for left/right audio channel |
| `4` aka `BCLK`   | `SCK`                 | Data clock                             |

## Connection to Main Computer

The audio board is permanently connected to the main computer by USB.
It appears as a USB audio device on the main computer.
In the systems settings the audio device can be selected for input and output.
So that all kinds of programms can make use of it.
But also custom robot programs can access these devices for audio input/output.

Audio input and output is transferred between audio borad and main computer via USB.
But the main computer can also use this USB connection to reprogram the audio board (Teensy 4.1).
For examples noise filters and other enhancers might be added to the audio processing.

## Board

The board is directly screwed to the `Head Carrier` inside the head to keep I<sup>2</sup>S lines as short as possible.

<img src="./board.png"/>

## Microphone

The microphone (breakout board) is secured inside the microphone case with a strip of foam.

Maybe I should have used heat shrink tubing on the solder joints, just to be sure.

<img src="./mic_insulated_with_foam.jpg"/>

The cables are secured by cable ties.

<img src="./mic_case.jpg"/>

The microphone case is mounted to the head.
The breakout board is visible from the outside if you take a closer look.

<img src="./mic_breakout_board.jpg"/>
