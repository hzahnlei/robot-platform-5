# Main Computer

I am using an NVIDIA Jetson Nano Developer Kit as main computer.
It is capable of hardware-accelerated deep learning and inference.

## WLAN/Bluetooth Module

I am using an Intel 8265 Desktop Wireless M.2 Kit to add WLAN funtionality to the on-board Jetson Nano.
It is not stated clearly which antenna plugs into which antenna connector.
Here is what I found out (see https://community.intel.com/t5/Wireless/Intel-8265NGW-antenna-connectors/m-p/556862 and https://www.intel.de/content/www/de/de/support/articles/000031221/wireless.html):

| Connector                  | Antenna |
|----------------------------|--------:|
| Antenne 1 (Aux) Wi-Fi + BT | 2.4GHz  |
| Antenne 2 (Main) Wi-Fi     | 5.0GHz  |

I am not using the antennas provided by Intel.
Instead I am using a much smaller PCB antenna.
Ofcause it does not offer the same performance as the bigger Intel antennas.
Maybe I have to change that later.

<img src="./intel_8265_wlan.png" style="hight: 50%"/>

<img src="./jetson_mounted_on_slidein.png" style="hight: 50%"/>

## Cooling Fan

The Jetson Nano comes with passive cooling, that is, no fan attached.
It can get quite hot.
I decided to add a cooling fan.
An abundance of such fans is offered by the usual suspects.

Setting the max speed of the fan can be achieved like so (see https://forums.developer.nvidia.com/t/the-fan-is-not-running-while-hot-and-there-is-no-access-to-the-fan-speed-sysfs-handle/72707/5):

```shell
sudo sh -c 'echo 255 > /sys/devices/pwm-fan/target_pwm'
```

It can be switched of like so:

```shell
sudo sh -c 'echo 0 > /sys/devices/pwm-fan/target_pwm'
```
