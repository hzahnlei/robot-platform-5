# Controller Board

## Introduction

Purpose of the controller board is to read sensors and to control actuators.

Examples for sensors are:

- Infrared distance sensor
- Limit switch
- Rotary encoder for measuring rotation of head

Examples for actuators are:

- Stepper motor for rotating the head
- Driving motors

More complex I/O, like audio, is driven by a Teensy on the audio board, or directly connected to the main computer (depth and tracking cameras).

## About the Microcontroller

I still like to use classic Arduinos (eight bit AVRs) for control tasks.
These are the reasons:

- They are well understood
- They are well documented
- Their compute power and memory capacity is absolutely sufficient for most control tasks
- A large software library exists
- Easy to handle

For these reasons I am using a Microchip ATmega2560 based microcontroller board.
It has plenty of I/O pins.
I am not using an original Arduino Mega 2560 though, because not all pins are within the 2.54mm raster.
Instead I am using a clone called "Mega 2560 Pro".
It has a smaller form factor, but more importantly, all pins are in the 2.54mm raster.
Hence, I can easily mount it on a prototype board.

## Inductive End-Switches

Three inductive end-switches are used to determine the position of the rotatable head.
They are positioned at fixed positions.
The switches are mounted to the body at 0° in the front, 90° to the right and -90° to the left.

The rotating part of the head carries simple screws at 0°, -60°, -90°, 60° and 90°.
If the head rotates those screws move over the swithces and trigger them.

The signal of the inductive end-switches is somewhat inverted.
0V means that the switch has been triggered.
Often interrupt inputs are firering on 0V or falling edge.
Hence the decision to invert the signal.

| Voltage | Description                            |
| ------: | -------------------------------------- |
| 0V      | The end-switch has been triggered.     |
| 5V      | The end-switch has not been triggered. |

The following diagram shows the position of the inductive end-switches on the body.

<img src="./position_of_end_switches.svg" width="25%"/>

The following diagram shows the position of the triggers (metal screws) inside the head that trigger the inductive end-switches.

<img src="./position_of_end_switch_triggers.svg" width="25%"/>

The end-switches, like other components, actually require >6V supply voltage.
Hence, `PSU1` is configured to provide 7.5V.
However, this voltage is to high to be fed into the Arduino.
Therefore the output of the end-switch is fed into a voltage devider.
The devider is made up of one 100k&#8486; and one 180k&#8486; resistor.
This way the high level is about 4.8V.

## Rotary Encoder

The rotary encoder, like other components, actually require >6V supply voltage.
Hence, `PSU1` is configured to provide 7.5V.

The encoder has an open-collector output.
Eventhough the encoder itself is fed with 7.5V we are using 5V provided by the Arduino to the open-collector.
Two 10k&#8486; resistors are used to limit the current to be fed into the encoder and Arduino inputs.

## Schematics

<img src="./controller_board.svg" width="80%"/>

## Ports Utilization

| ATmega2560 Port    | Arduino Name    | Utilized As                                            |
| ------------------ | --------------- | ------------------------------------------------------ |
|                    |                 | **End-Switches/Head**                                  |
| `PE4` aka `INT:4`  | `D2`            | Interrupt input of right inductive end-switch of head  |
| `PE5` aka `INT:5`  | `D3`            | Interrupt input of middle inductive end-switch of head |
| `PD3` aka `INT:3`  | `D18`           | Interrupt input of left inductive end-switch of head   |
|                    |                 | **Rotary Encode/Head**                                 |
| `PD2` aka `INT:2`  | `D19`           | Interrupt input of signal A of rotary encoder of head  |
| `PD0` aka `INT:0`  | `D21`           | Interrupt input of signal B of rotary encoder of head  |
|                    |                 | **Motor Controller/Head**                              |
| `PK0` aka `ADC:8`  | `D62` aka `A8`  | Pulse                                                  |
| `PK1` aka `ADC:9`  | `D63` aka `A9`  | Direction                                              |
| `PK2` aka `ADC:10` | `D64` aka `A10` | Enable (holding current)                               |
|                    |                 | **Motor Controller/Left Drive Motor**                  |
| `PK3` aka `ADC:11` | `D65` aka `A11` | Pulse                                                  |
| `PK4` aka `ADC:12` | `D66` aka `A12` | Direction                                              |
| `PK5` aka `ADC:13` | `D67` aka `A13` | Enable (holding current), *also for right drive motor* |
|                    |                 | **Motor Controller/RIGHT Drive Motor**                 |
| `PK6` aka `ADC:14` | `D68` aka `A14` | Pulse                                                  |
| `PK7` aka `ADC:15` | `D69` aka `A15` | Direction                                              |

## Board

<img src="./board.png"/>

## A Problem With the Mega 2560 Board

The main computer did not start, when the controller board was connected to the USB port of the main computer.
Schematics for this Arduino clone are not available.
But it seems there is no transistor on the board to separate 5V from USB and 5V generated by the board when supplied externally.
This transistor is present on original Ardunio Mega 2560 boards.
As a result, 5V generated by the controller board is connected to the 5V line of the USB port.

- Of cause I want controller board and main computer to be connetcted permanently via USB.
  - Main computer sending commands to controller board and receiving sensor data from controller board.
  - Main computer uploading programs to controller board.
- Also, I do not want to power controller board via USB bit via external supply.
  - All microcontrollers should be on independent of main computer.
    This way, MCUs can put actuators to safe modes etc.
  - I don't want to put the burden to supply all circuitry to the main computer's power regulators.

My solution to this was:

- Take a USB 2.0 cable and cut it in half.
- Reconnect all lines (GND and data) except for the 5V line.

This way, USB 5V and Arduino 5V are decoupled.

<img src="./usb_cable_1.jpg"/>

<img src="./usb_cable_2.jpg"/>

<img src="./usb_cable_3.jpg"/>
