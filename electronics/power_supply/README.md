# Power Supply

## Introduction

The idea behind the power wiring is:

-  A general switch (`SW1`) totally disconnects all electronics from the battery.
(The battery is a ca. 25.5V Lithium-ion battery.)
However, as soon as switched on, all microcontrollers are getting powered, so that they can initialize and assume a safe mode.
-  The main computer is powered with switch `SW2`.
Meaning, the "intelligence" can be switched on/off separately from the microcontrollers.
-  Switch `SW3` powers the motor controllers (`MC1`, `MC2` and `MC3`).
Meaning, the motors can be shut-down in case of emergency without shutting down the main computer and microcontrollers.
The motor controllers are directly attached to the battery (if `SW1` and `SW3` are closed).
They are powered at 25V and can draw whatever current they need.
The current though is limited by the motor controller to meet the motor's specifications.
-  Power supply `PSU1` is a DC/DC converter that delivers 2A/7.5V.
The microcontrollers are equipped with DC/DC converters that finally deliver the voltages required by the respective controller.
-  Power supply `PSU2` is a DC/DC converter that delivers 4A/5V.
5V is the voltage that is required by the main computer.
Like the microcontrollers it has its own DC/DC converters that generate all voltages required from that single input voltage.

The next digram shows a high level view of the power wiring.
Details such as connectors are omitted to keep the overview.

<img src="./power_wiring_overview.svg" width="40%"/>

This is the complete power wiring.
It contains all details such as connectors, switches and screw terminals.
Find further descriptions in the following sections below.

<img src="./complete_power_wiring.svg" width="80%"/>

## Wires

All wires are relatively thick.
They are at least 0.75mm<sup>2</sup>.
Some are even 1.5mm<sup>2</sup>.

## Charging Plug

The battery pack is equiped with a barrel jack `CN1` for charging.
It accepts center positive polarity plugs that are 9.5mm deep, with 2.1mm inner diameter and 5.5mm outer diameter.

It comes with a charger that is equiped with such a center positive plug.
The charger provides 25.2VDC and 2A.

## Main Power Switch Connector

Main power plug `CN2a` is directly connected to the battery pack.
`CN2a` plugs into `CN2b` and is directly connected to switch `SW1`.
When `SW1` is closed the screw terminals `ST1a` (GND) and `ST1b` (GND) and `ST2` (+25V) are connected to the battery pack.
GND and battery power can be distributed from `ST1a`, `ST1b` and `ST2`.

`PSU1` us connected to `ST1b` and `ST2`.
All computers that perform control task are powered up immediatly with `SW1` on.
It is required that, computers that perform control task, are initializing and putting all actuators etc. into a safe state.
For example: Drive motors must not be powered.
The robot must not move.

## Main Computer Switch Connector

Switch `SW2` is connected via `CN2a` to `ST1b` and `ST2`.

`PSU2` is connected to `ST1b` and `ST2`.
The main computer (and USB hub) is switched on/off with `SW2`.
The main computer is only supplied with power when `SW1` **and** `SW2` are switched on.

## Actuator Switch Connector

Switch `SW3` is connected via `CN3a` to `ST1b` and `ST2`.
Actuators are only supplied with power when `SW1` **and** `SW3` are switched on.
`SW3` can be used as a safety-feature to power-down actuators without shutting down the main computer or the microcontrollers.

## Main Computer Power Supply

A Buck converter (`PSU2`) is connected to screw terminals `ST1a`, `ST1b` and `ST2`.
It delivers (in theory) up to 300W and is configured for 5.0V.

`PSU2` outputs its 5.0V to screw terminal `ST4`.

`CN4a`, an XT60 plug, is attached to `ST4` and `ST1b` and intended to power the main computer (Nvidia Jetson Nano).

`CN17a`, an XT60 plug, is attached to `ST4` and `ST1b` and intended to power the USB hub.
High-bandwidth USB devices are directly connected to the main computer.
(Examples are depth and tracking cameras and audio devices.)
However, the main computer only has four USB ports.
This is why an USB hub has been added.
Microcontrollers (performing simple control tasks) are connected to the hub.

## Control Computer Power Supply

A Buck converter (`PSU1`) is connected to screw terminals `ST1a`, `ST1b` and `ST2`.
It delivers (in theory) up to 300W and is configured for 7.5V.
The control computers (Arduinos) have their own regulators to produce 5.0V and 3.3V from this single 7.5V input.
Some sensors (inductive end-switch for example) require 6V &le; v &le; 36V.
These can be connected to 7.5V.

`PSU1` outputs its 7.5V to screw terminal `ST3`.

`CN4a`, an XT60 jack, is attached to `ST3` and `ST1b` and intended to power an Arduino microcontroller board.
(It controls actuators and reads low-bandwidth sensors.)

`CN5a`, an XT60 jack, is attached to `ST3` and `ST1b` and intended to power a Teensy microcontroller board.
However, this acts as an USB audio device and does not need to be on until the main computer is on.
Hence, It is currently powerd by the main computer via USB.
`CN5a` is not used currently.

## Pictures

This is a picture of building the power pack.
Hard to see is the 25V battery.
The buck converters do reside on the left and right sides.
Screw terminals are used to disribute GND and different voltage levels.

<img src= "./building_the_power_pack.jpg"/>

For testing my "power pack" I went outside.
I did not trust it and did not want to risk a lithium ion battery blow up in my basement.

<img src= "./testing_the_power_pack.jpg"/>

The first time the switches lid up.

<img src= "./switches_lid_1st_time.jpg"/>

This is the first time the main computer ran on batteries.

<img src= "./main_computer_on_battery_power_1st_time.jpg"/>

## Appendix: Barrel Jacks and Plugs

Barrel jacks and plugs come in two combinations of polarity.

If the plug has positive center polarity, then the inner contact has a positive voltage while the outer contact is GND.
The barrel jack has to be wired accordingly.

<img src="positive_center_polarity.svg" width="20%"/>

If the plug has negative center polarity, then the inner contact is GND while the outer contact has a positive voltage.
The barrel jack has to be wired accordingly.

<img src="negative_center_polarity.svg" width="20%"/>
